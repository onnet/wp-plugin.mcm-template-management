<?php

/**
 * Fired during plugin activation
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 * @author     Hyve Mobile <glen@hyvemobile.co.za>
 */
class Mcm_Template_Management_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
