<?php
//if (!function_exists('display_theme_banner_preview')):
//    /**
//     * @param int    $attachment_id
//     * @param string $button_text
//     *
//     * @return bool
//     */
//    function display_theme_banner_preview($attachment_id, $button_text)
//    {
//        if ($attachment_id > 0) {
//            $attachment = wp_get_attachment_metadata($attachment_id);
//            $upload_dir = wp_upload_dir();
//            if (is_array($attachment) && isset($attachment['file'])) {
//                $content_url = $upload_dir['baseurl'] . DIRECTORY_SEPARATOR . $attachment['file'];
//                $alt = get_the_title($attachment_id);
//                echo "<img width=\"266\" class=\"attachment-266x266 size-266x266\" alt=\"{$alt}\" src=\"{$content_url}\" srcset=\"\" sizes=\"(max-width: 266px) 100vw, 266px\">";
//
//                return true;
//            }
//        }
//
//        // Display button text
//        _e($button_text);
//
//        return false;
//    }
//endif;
//
//if (!function_exists('wp_is_theme_really_saving')):
//    /**
//     * @param int    $post_id
//     * @param object $post
//     *
//     * @return bool
//     */
//    function wp_is_theme_really_saving($post_id, $post)
//    {
//        // If this is just a revision, don't send the email.
//        if (wp_is_post_revision($post_id)) {
//            return false;
//        }
//
//        if (isset($post->post_status) && in_array($post->post_status, ['trash', 'auto-draft'])) {
//            return false;
//        }
//
//        if (isset($_GET['action']) && in_array($_GET['action'], ['untrash', 'trash'])) {
//            return false;
//        }
//
//        return true;
//    }
//endif;
//
//if (!function_exists('include_partial')):
//    /**
//     * Include a partial to use in our admin functions
//     *
//     * @param string $slug the first part of the filename
//     * @param string $name the second part of the filename
//     * @param array  $args
//     */
//    function include_partial($slug = '', $name = '', $args = [])
//    {
//
//        dd(plugin_dir_path());
//        extract($args);
//        /** @noinspection PhpIncludeInspection - This is a dynamic file inclusion */
//        include plugin_dir_path(__FILE__) . '/partials/' . $slug . (empty($name) ? '' : '-' . $name) . '.php';
//    }
//endif;