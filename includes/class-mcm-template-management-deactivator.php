<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 * @author     Hyve M0bile <glen@hyvemobile.co.za>
 */
class Mcm_Template_Management_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
