<?php
/** @noinspection PhpIncludeInspection */

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/includes
 * @author     Hyve Mobile <glen@hyvemobile.co.za>
 */
class Mcm_Template_Management
{

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Mcm_Template_Management_Loader $loader Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $plugin_name The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string $version The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
        $this->version = defined('template_management_VERSION') ? template_management_VERSION : '1.0';
        $this->plugin_name = 'mcm-template-management';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - template_management_Loader. Orchestrates the hooks of the plugin.
     * - template_management_i18n. Defines internationalization functionality.
     * - template_management_Admin. Defines all hooks for the admin area.
     * - template_management_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies()
    {
        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-template-management-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-template-management-i18n.php';

        /**
         * Required Helper files
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-mcm-template-management-helpers.php';
        foreach (glob(plugin_dir_path(dirname(__FILE__)) . 'includes/helpers/*.php') as $filename) {
            require_once "{$filename}";
        }

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-mcm-template-management-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-mcm-template-management-admin-settings.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-mcm-template-management-public.php';

        $this->loader = new template_management_Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the template_management_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale()
    {
        $plugin_i18n = new template_management_i18n();
        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks()
    {
        $plugin_admin = new Mcm_Template_Management_Admin($this->get_plugin_name(), $this->get_version());
//        $plugin_admin_settings = new Mcm_Template_Management_Admin_Settings($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        // Add Theme Actions
//        $this->loader->add_action('init', $plugin_admin, 'theme_post_type');
//        $this->loader->add_action('add_meta_boxes', $plugin_admin, 'register_meta_boxes');
//        $this->loader->add_action('save_post_custom_theme', $plugin_admin, 'save_post', 10, 3);
//        $this->loader->add_action('wp_trash_post', $plugin_admin, 'remove_custom_theme_post_id');
//        $this->loader->add_action('service_theme_redirect', $plugin_admin, 'service_theme_redirect');

        // Settings Actions
//        $this->loader->add_action('admin_menu', $plugin_admin_settings, 'add_options_page');
//        $this->loader->add_action('admin_init', $plugin_admin_settings, 'register_service_settings');
//        $this->loader->add_action('admin_init', $plugin_admin_settings, 'update_option_hooks');
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks()
    {
        $plugin_public = new Mcm_Template_Management_Public($this->get_plugin_name(), $this->get_version());

        // Add Theme Public Actions
//        $this->loader->add_action('theme_styles', $plugin_public, 'theme_styles', 11);
//        $this->loader->add_action('display_theme_banner', $plugin_public, 'display_theme_banner');
//        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles', 98);
//        $this->loader->add_action('wp_head', $plugin_public, 'theme_styles', 99);
//        $this->loader->add_action('template_redirect', $plugin_public, 'set_theme_banner_id');
//        $this->loader->add_action('set_theme_query_vars', $plugin_public, 'set_theme_query_vars');

        // Add Theme Public Filters
//        $this->loader->add_filter('query_vars', $plugin_public, 'add_query_vars', 10);
//        $this->loader->add_filter('mcm_set_theme_hexes', $plugin_public, 'mcm_set_theme_hexes', 10);
//        $this->loader->add_filter('get_theme_banner', $plugin_public, 'get_theme_banner', 10, 2);
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run()
    {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name()
    {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    template_management_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader()
    {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version()
    {
        return $this->version;
    }

}
