<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://bitbucket.org/onnet/wp-plugin.mcm-campaign-management
 * @since      1.0.0
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Mcm_Template_Management
 * @subpackage Mcm_Template_Management/public
 * @author     Hyve MObile <glen@hyvemobile.co.za>
 */
class Mcm_Template_Management_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     *
     * @param      string $plugin_name The name of the plugin.
     * @param      string $version     The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Add club processing query vars
     *
     * @param array $query_vars
     *
     * @return array
     */
    function add_query_vars($query_vars)
    {
        return $query_vars;
    }
}
