<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/onnet/wp-plugin.mcm-template-management
 * @package           Mcm_Template_Management
 *
 * @wordpress-plugin
 * Plugin Name:       MCM Template Management
 * Plugin URI:        https://bitbucket.org/onnet/wp-plugin.mcm-template-management
 * Description:       Enables template management discovery
 * Version:           1.0
 * Author:            Hyve Mobile
 * Author URI:        https://bitbucket.org/onnet/wp-plugin.mcm-template-management
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       mcm-template-management
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
defined('WPINC') or die();

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('template_management_VERSION', '1.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mcm-template-management-activator.php
 */
function activate_template_management()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-mcm-template-management-activator.php';
    Mcm_Template_Management_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mcm-template-management-deactivator.php
 */
function deactivate_template_management()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-mcm-template-management-deactivator.php';
    Mcm_Template_Management_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_mcm_template_management');
register_deactivation_hook(__FILE__, 'deactivate_mcm_template_management');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-mcm-template-management.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_template_management()
{
    $plugin = new Mcm_Template_Management();
    $plugin->run();
}
run_template_management();